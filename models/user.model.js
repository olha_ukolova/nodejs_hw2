const mongoose = require('mongoose');

const User = mongoose.model(
    'User',
    new mongoose.Schema({
      username: {
        type: String,
        unique: true,
        required: true,
      },
      password: {
        type: String,
        required: true,
      },
      createdAt: {
        type: Date,
        default: Date.now(),
      },
      notes: [
        {
          _id: String,
          userId: String,
          completed: Boolean,
          text: String,
          createdDate: Date,
        },
      ],
    } ),
);

module.exports = User;
