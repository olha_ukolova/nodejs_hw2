require('dotenv').config({path: './.env'});

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const {authJwt} = require('./middlewares');

const dbConfig= {
  HOST: process.env.HOST,
  DB: process.env.DB
};

const PORT = process.env.PORT;

const app = express();


app.use(cors());
app.use(morgan('combined'));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}));

const db = require('./models');


const authRoute = require('./routes/auth.routes');
const userRoute = require('./routes/user.routes');
const noteRoute = require('./routes/notes.routes');

app.use('/api/auth', authRoute);
app.use('/api/users', authJwt, userRoute);
app.use('/api/notes', authJwt, noteRoute);


const initialize = async () => {
  try {
    await db.mongoose.connect(`${dbConfig.HOST}${dbConfig.DB}?retryWrites=true&w=majority`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    app.listen(PORT);
  } catch (err) {
    console.error(`Failed to start server: ${err.message}`);
    return;
  }
  console.log('server is running at port ' + PORT);
  console.log('Successfully connect to MongoDB.');
};

initialize();
