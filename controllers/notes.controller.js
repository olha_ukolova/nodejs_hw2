const Note = require('../models/').note;
const User = require('../models/').user;


exports.getNotes = async (req, res) => {
  try {
    const offset = +(req.query.offset ?? 0)
    const limit = +(req.query.limit ?? 0)
    const count = await Note.count({userId: req.userId});
    const notes = await Note.find({userId: req.userId}, '-__v')
        .skip(+offset).limit(+limit);
    let notelist = []
      notelist = notes.map((el)=>{
        return ({
          _id: el._id,
          userId: el.userId,
          completed: el.completed,
          text: el.text,
          createdDate: el.createdDate,
        });
      });
      res.status(200).send({
        offset: offset,
        limit: limit,
        count,
        notes: notelist,
      });
  } catch (error) {
    res.status(500).send({message: 'Internal server error'});
  };
};

exports.getOneNote = async (req, res) => {
  try {
    const note = await Note.findOne({_id: req.params.id});
    if (!note||note.userId.toString() !== req.userId.toString()) {
      res.status(400).send({message: 'You can\'t see this note info'});
    } else {
      res.status(200).send({
        note: {
          _id: note._id,
          userId: note.userId,
          completed: note.completed,
          text: note.text,
          createdDate: note.createdDate,
        },
      });
    }
  } catch (error) {
    res.status(400).send({message: 'Nothing found'});
  }
};

exports.checkNote= async (req, res) => {
  try {
    const note = await Note.findOne({_id: req.params.id});
    if (!note||note.userId.toString() !== req.userId.toString()) {
      res.status(400).send({message: 'You can\'t work with this note'});
    } else {
      await Note.findByIdAndUpdate({_id: req.params.id},
          {completed: !note.completed});
      res.status(200).send({
        message: 'Success',
      });
    }
  } catch (error) {
    res.status(400).send({message: 'Note doesn\'t found'});
  }
};

exports.updateOneNote= async (req, res) => {
  try {
    const {text} = req.body;
    const note = await Note.findOne({_id: req.params.id});
    if (!note||note.userId.toString() !== req.userId.toString()) {
      res.status(400).send({message: 'You can\'t work with this note'});
    } else {
      await Note.findByIdAndUpdate({_id: req.params.id}, {text: text});
      res.status(200).send({
        message: 'Success',
      });
    }
  } catch (error) {
    res.status(400).send({message: 'Note doesn\'t found'});
  }
};

exports.deleteOneNote= async (req, res) => {
  try {
    const note = await Note.findOne({_id: req.params.id});
    if (!note||note.userId.toString() !== req.userId.toString()) {
      res.status(400).send({message: 'You can\'t work with this note'});
    } else {
      await Note.deleteOne({_id: req.params.id});
      res.status(200).send({
        message: 'Success',
      });
    }
  } catch (error) {
    res.status(400).send({message: 'Note doesn\'t found'});
  }
};

exports.createNote = async (req, res) => {
  try {
    const user = await User.findOne({_id: req.userId});
    const {text} = req.body;
    const note = await new Note({
      userId: req.userId,
      text,
    });
    if (user) {
      await note.save();
      res.status(200).send({message: 'Success'});
    } else {
      res.status(400).send({message: 'User doesn\'t found!'});
    }
  } catch (error) {
    res.status(400).send({message: 'Note doesn\'t found'});
  }
};

