require('dotenv').config({path: '../.env'});
const config = process.env.SECRET;
const db = require('../models');
const User = db.user;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.register = (req, res) => {
  if (req.body.password&&req.body.password) {
    const user = new User({
      username: req.body.username,
      password: bcrypt.hashSync(req.body.password, 8),
    });

    user.save((err) => {
      if (err) {
        if (!err.status) {
          res.status(500).send({message: 'Internal server error'});
        } else {
          res.status(400).send({message: err.message});
        }
      } else {
        res.status(200).send({message: 'Success'});
      }
    });
  } else {
    res.status(400).send({message: 'Password Not found.'});
  }
};

exports.login = (req, res) => {
  if (req.body.password&&req.body.password) {
    User.findOne({
      username: req.body.username,
    })
        .exec((err, user) => {
          if (err) {
            if (!err.status) {
              res.status(500).send({message: 'Internal server error'});
            } else {
              res.status(400).send({message: err.message});
            }
          }

          if (!user) {
            return res.status(400).send({message: 'User Not found.'});
          }

          const passwordIsValid = bcrypt.compareSync(
              req.body.password,
              user.password,
          );

          if (!passwordIsValid) {
            return res.status(400).send({
              message: 'Invalid Password!',
            });
          }

          const token = jwt.sign({id: user.id}, config, {
            expiresIn: 86400,
          });

          res.status(200).send({
            message: 'Success',
            jwt_token: token,
          });
        });
  } else {
    res.status(400).send({message: 'Password Not found.'});
  }
};
