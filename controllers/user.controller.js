const User = require('../models/').user;
const bcrypt = require('bcryptjs');


exports.userInfo = async (req, res) => {
  try {
    const user = await User.findById({_id: req.userId});
    res.status(200).send({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdAt,
      },
    });
  } catch (error) {
    res.status(400).send({message: 'User not found'});
  }
};

exports.deleteUser = async (req, res) => {
  try {
    const user = await User.findOne({_id: req.userId});
    if (!user) {
      res.status(400).send({message: 'User not found'});
    } else {
      await User.findByIdAndRemove({_id: req.userId});
      res.status(200).send({message: 'Success'});
    }
  } catch (error) {
    if (!error.status) {
      res.status(500).send({message: 'Internal server error'});
    } else {
      res.status(400).send({message: error.message});
    }
  }
};

exports.changePassword = async (req, res) => {
  User.findById({_id: req.userId}, async (err, data) => {
    try {
      if (!data) {
        res.status(400).send({
          'message': 'There is no user with such id.',
        });
      }
      if (await bcrypt.compare(req.body.oldPassword, data.password)) {
        data.password = bcrypt.hashSync(req.body.newPassword, 8);
        data.save();
        res.status(200).send({
          'message': 'Success',
        });
      } else {
        res.status(400).send({
          'message': 'Old password isn\'t correct.',
        });
      }
    } catch (err) {
      res.status(500).send({
        'message': 'Internal server error',
      });
    }
  });
};

