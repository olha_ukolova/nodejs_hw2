const express = require('express');
const controllers = require('../controllers/notes.controller');

const router = express.Router();


router.get('/', controllers.getNotes);
router.post('/', controllers.createNote);
router.get('/:id', controllers.getOneNote);
router.delete('/:id', controllers.deleteOneNote);
router.patch('/:id', controllers.checkNote);
router.put('/:id', controllers.updateOneNote);



module.exports = router;
