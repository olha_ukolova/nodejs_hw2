const controller = require('../controllers/user.controller');

const express = require('express');

const router = express.Router();

router.get('/me', controller.userInfo);

router.delete('/me', controller.deleteUser);

router.patch('/me', controller.changePassword);

module.exports = router;
