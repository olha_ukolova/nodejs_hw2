require('dotenv').config({path: '../.env'});
const jwt = require('jsonwebtoken');

const config = process.env.SECRET;


verifyToken = (req, res, next) => {
  const tokenHead = req.headers['authorization'];
  let bearer;
  let bearerToken;
  if (!tokenHead) {
    return res.status(400).send({message: 'No token provided!'});
  } else {
    bearer = tokenHead.split(' ');
    bearerToken = bearer[1]??bearer[0];
  }

  if (!bearerToken) {
    return res.status(400).send({message: 'No token provided!'});
  }

  jwt.verify(bearerToken, config, (err, decoded) => {
    if (err) {
      return res.status(400).send({message: 'Unauthorized!'});
    }
    req.userId = decoded.id;
    req.userName = decoded.username;
    next();
  });
};

const authJwt = verifyToken;

module.exports = authJwt;
